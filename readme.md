# Instrucciones
Si ya tienes homestead instalado pasa al punto 2
1. Instalación de composer y node.js
	* Deberás instalar composer siguiendo las instrucciones del siguiente link: [Composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
	* Deberás también tener instalado el instalador de paquetes npm que es parte de node.js, siguiendo las instrucciones del siguiente link: [Node.js](https://nodejs.org/es/download/package-manager/)
2. En tu homestead que se instala siguiendo las instrucciones del link: [Laravel Homestead - Laravel - The PHP Framework For Web Artisans](https://laravel.com/docs/5.6/homestead#installation-and-setup) da de alta un nuevo sitio y una base de datos editando el archivo Homestead.yaml, ejemplo:
```
sites:
	- map: ticker.test
    to: /var/www/bitsoticker/public
    schedule: true

databases:
	- bitsoticker_test
```
Es importante activar schedule puesto que la aplicación web requiere de tareas programas que funcionan con crontabs.
> En caso de que no tengas instalado homestead y hayas instalado por separado composer, y node.js, es necesario que crear una base de datos MySQL en tu servidor de base de datos y editar el crontab con el comando:  
```
	crontab -e
```
> desde tu terminal o línea de comandos y agregar la siguiente línea:  
```
* * * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1
```
> para que se puedan ejecutar las tareas programadas del proyecto  

Ya teniendo los ambientes listos, es necesario tener en el proyecto el archivo .env con los siguientes parámetros:
```
APP_NAME="Bitso Ticker"
APP_ENV=local
APP_KEY=SE-GENERA-CON-EL-COMANDO: php artisan key:generate
APP_DEBUG=true
APP_URL=TU-URL-DE-SITIO-TESTING, ejemplo http://ticker.test
APP_LANG=es
APP_TIMEZONE="America/Mexico_City"


LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=TU-HOST-O-IP-DE-SERVIDOR-DE-BASE-DE-DATOS
DB_PORT=3306
DB_DATABASE=NOMBRE-DE-TU-BASE-DE-DATOS
DB_USERNAME=NOMBRE-DE-USUARIO-DE-BASE-DE-DATOS
DB_PASSWORD=CONTRASEÑA-DE-USUARIO-DE-BASE-DE-DATOS

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
SESSION_LIFETIME=120
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

API_BITSO_URL=https://api.bitso.com/v3/
```

Lo siguiente será ejecutar los comandos:
```
composer install
npm install
php artisan migrate
npm run dev
```

> Si no tienes homesdead, debes ejecutar el comando:  
```
php artisan serve
```
> El comando lo que hará es iniciar un servidor en el puerto 8000 de tu localhost o 127.0.0.1  