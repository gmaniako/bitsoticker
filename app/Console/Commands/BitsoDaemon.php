<?php

namespace App\Console\Commands;

use Illuminate\Console\Command,
    GuzzleHttp\Client,
    App\Entities\BitsoPrice;

class BitsoDaemon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bitso:daemon';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Bitso Daemon para solicitud de precio de Bitcoin';

    public $base_url;

    protected $client;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Client $client)
    {
        parent::__construct();
        $this->client = $client;
        $this->base_url = env('API_BITSO_URL');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ticker_url = $this->base_url . "ticker?book=btc_mxn";

        // Realizamos el request con Guzzle
        $request = $this->client->request('GET', $ticker_url);

        // Revisamos que la respuesta de la petición sea un status 200 de lo contrario guardamos un mensaje en el log
        if ( $request->getStatusCode() != 200 )
            return $this->comment('No se pudieron obtener datos');

        // Parseamos los datos de respuesta
        $response = json_decode($request->getBody());
        $success = $response->success;
        
        // Verificamos que la respuesta a pesar de haber tenido un status 200, también tenga un success true
        if ( ! $success )
            return $this->comment('Existió un problema al realizar la petición');

        $payload = $response->payload;

        // Guardamos los datos en base de datos
        BitsoPrice::create([
            'last'              => $payload->last,
            'high'              => $payload->high,
            'low'               => $payload->low,
            'date'              => $payload->created_at
        ]);
    }
}
