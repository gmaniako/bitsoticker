<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BitsoController extends Controller
{
    /**
     * Método para mostrar la vista inicial
     *
     * @return void
     */
    public function index() {
        return view('index');
    }
}
