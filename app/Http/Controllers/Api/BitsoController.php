<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller,
    App\Entities\BitsoPrice;

class BitsoController extends Controller
{
    /**
     * Método para obtener los últimos 30 registros del precio del Bitcoin
     *
     * @return void
     */
    public function index() {
        $prices = BitsoPrice::latest()->take(30)->get()->toArray();
        return \array_reverse($prices);
    }
}
