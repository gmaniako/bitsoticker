<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model,
    Carbon\Carbon;

class BitsoPrice extends Model
{
    public $timestamps = true;

    protected $table = 'bitso_prices';

    protected $fillable = [
        'book',
        'high',
        'low',
        'last',
        'date'
    ];

    protected $hidden = [];

    protected $dates = [];

    public function getCreatedAtAttribute($created_at) {
        return Carbon::parse($created_at)->format('H:i');
    }

    public function getHighAttribute($high) {
        return \number_format(\preg_replace('/[^\d\.]/', '', (float)$high), 2);
    }

    /*public function getLastAttribute($last) {
        return \number_format(\preg_replace('/[^\d\.]/', '', (float)$last), 2);
    }*/

    public function getLowAttribute($low) {
        return \number_format(\preg_replace('/[^\d\.]/', '', (float)$low), 2);
    }
}
