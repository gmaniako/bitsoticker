<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBitsoPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitso_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('book', ['btc_mxn', 'eth_mxn', 'xrp_mxn', 'ltc_mxn'])->default('btc_mxn')->comment('Tipo de Criptomoneda');
            $table->double('last', 16, 4)->default(0.0000)->nullable()->comment('Precio');
            $table->double('high', 16, 4)->default(0.0000)->nullable()->comment('Precio más alto');
            $table->double('low', 16, 4)->default(0.0000)->nullable()->comment('Precio más bajo');
            $table->string('date')->nullable()->comment('Fecha de obtención');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitso_prices');
    }
}
