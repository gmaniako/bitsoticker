@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12" ng-controller="chartController" ng-init="getPrices();" style="margin-top: 70px;">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-left"><i class="fa fa-bitcoin"></i> Precio de Bitcoin BTC/MXN</h3>
                </div>
                <div class="card-body">
                    <canvas id="bitso-chart" class="chart chart-line" chart-series="series" chart-data="prices" chart-labels="labels" chart-options="options" height="500px"></canvas>
                </div>
                <div class="card-footer text-muted">
                    Los datos son actualizados automáticamente cada 5 minutos
                </div>
            </div>
        </div>
    </div>
@endsection