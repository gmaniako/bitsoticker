'use strict';

var app = angular.module('bitcoinApp', ['chart.js'])
	.config(['$httpProvider', function($httpProvider){
		$httpProvider.defaults.headers.post['_token'] = $('meta[name="csrf-token"]').attr('content');
		$httpProvider.defaults.paramSerializer = '$httpParamSerializerJQLike';
	}]).config(['$qProvider', function($qProvider){
		return $qProvider.errorOnUnhandledRejections(false);
	}]).config(['ChartJsProvider', function (ChartJsProvider){
		ChartJsProvider.setOptions({
			'chartColors': ['#07989c', '#06888c', '#06787b', '#05686b', '#04595b'],
			'maintainAspectRatio': false,
			'responsive': true
		});
	}]).controller('chartController', ['$scope', '$http', function($scope, $http) {
		$scope.labels = null;
		$scope.prices = null;
		$scope.series = ['Bitcoin'];
		$scope.options = {
			scales: {
				yAxes: [
					{
						type: 'linear',
						display: true,
						position: 'left',
						ticks: {
							beginAtZero: false
						},
						scaleLabel: {
							display: true,
							labelString: 'Precio'
						}
					}
				]
			},
			xAxes: [
				{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Fecha'
					}
				}
			],
			tooltips: {
				mode: 'label',
				intersect: true
			}
		}

		/*
			Método para obtener el objeto de los precios desde la API
		*/
		$scope.getPrices = function() {
			$scope.labels = null;
			$scope.prices = null;
			let Labels = [];
			let Prices = [];
			$http.get('/api/bitso').then((function(response) {
				if ( response.data ) {
					response.data.forEach(element => {
						Labels.push(element.created_at);
						Prices.push(element.last);
					});

					$scope.labels = Labels;
					$scope.prices = Prices;
				}
			}), function(errors){
				console.log(errors);
			});
		}
	}]);