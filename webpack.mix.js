let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');*/
mix.scripts([
	'node_modules/jquery/dist/jquery.min.js',
	'node_modules/angular/angular.min.js',
	'node_modules/bootstrap/dist/bootstrap.min.js',
	'node_modules/chart.js/dist/Chart.min.js',
	'node_modules/angular-chart.js/dist/angular-chart.min.js',
	'resources/assets/js/angular/app.js'
], 'public/js/all.js', './')
	.sass('resources/assets/sass/app.scss', '../resources/assets/build/css/app.css')
	.styles([
		'node_modules/font-awesome/css/font-awesome.min.css',
		'resources/assets/build/css/app.css'
	], 'public/css/app.css', './')
	.copy('node_modules/font-awesome/fonts', 'public/fonts');
